import * as STATUS_CODES from 'http-status-codes';
import mysql from 'mysql';
import moment from 'moment';
import http from 'http';
import cloudinary from 'cloudinary';

let databaseCredentials;
databaseCredentials = require('./database-credentials.json');

cloudinary.config({ 
	cloud_name: 'national-school-project', 
	api_key: '982334382457715', 
	api_secret: 'V5K9_jbZOloxl-drGV-NM-rcDJA' 
  });

let siteUrl = process.env.MODE === 'production' ?
	'http://stories.nationalschoolproject.com'
	:
	process.env.MODE === 'test' ?
		'http://test.stories.nationalschoolproject.com'
		:
		`http://localhost:${process.env.PORT || 8083}`;

function typeCast(field, useDefaultTypeCasting) {
	if ((field.type === "BIT") && (field.length === 1)) {
		var bytes = field.buffer();
		return (bytes[0] === 1);
	}
	return (useDefaultTypeCasting());
}
function queryFormat (query, values) {
	if (!values) return query;
	return query.replace(/\:(\w+)/g, function (txt, key) {
		if (values.hasOwnProperty(key)) {
			return this.escape(values[key]);
		}
		return txt;
	}.bind(this));
}

//Initialize our database connections
const pool = mysql.createPool({
	user: databaseCredentials.user.user,
	password: databaseCredentials.user.pass,
	host: '66.35.83.208',
	database: 'califo_storycapture',
	typeCast,
	queryFormat
});
const adminPool = mysql.createPool({
	user: databaseCredentials.admin.user,
	password: databaseCredentials.admin.pass,
	host: '66.35.83.208',
	database: 'califo_storycapture',
	typeCast,
	queryFormat,
});

const studentDevelopers = [
	"mateo.d.langstonsmith@biola.edu",
	"tjyurek@gmail.com",
	"aaron.b.chan@biola.edu",
	"enle.xu@biola.edu",
	"noa.d.wilding@biola.edu",
	"kailynne.l.lopez@biola.edu",
	"mia.m.honcoop@biola.edu",
	"joseph.william.stewart@biola.edu"
];

export const getConnectionPool = () => pool;
export const getSiteUrl = () => siteUrl;
export const getHomeDirectory = () => __dirname;
export const getCloudinary = () => cloudinary;

/***
 * @description Takes a users email and determines if that user is a student developer
 */
export const isStudentDeveloper = email => studentDevelopers.includes(email);

/***
 * @description Takes an array and formats it to be used in a SQL query
 ***/
export const formatArrayForSQL = a => {
	if (a.length == 0) {
		a.push(-1);
	}
	return JSON.stringify(a).replace("[", "(").replace("]", ")");
}

/***
 * @description Wraps a callback function so that it can utilize a database connection
 ***/
export const withDBConnection = callback => {
	return (request, response) => {
		try {
			pool.getConnection(
				(error, connection) => {
					if (error) {
						return response.send(error);
					}
					else {
						return callback(request, response, connection);
					}
				}
			);
		}
		catch (e) {
			response.error(e.stringify());
		}
	}
}

/***
 * @description Wraps a callback function so that it can utilize an admin database connection
 ***/
export const withAdminDBConnection = callback => {
	return (request, response) => {
		try {
			adminPool.getConnection(
				(error, connection) => {
					if (error) {
						return response.send(error);
					}
					else {
						return callback(request, response, connection);
					}
				}
			);
		}
		catch (e) {
			response.error(e.stringify());
		}
	}
}

/***
 * @description Checks if user is logged in
 ***/
export const isLoggedIn = (request,response,next) => {
	if (request.isAuthenticated() || process.env.NO_AUTH) {
		return next();
	}

	response.status(STATUS_CODES.UNAUTHORIZED).json({reason: 'You are not logged in'});
}

/***
 * @description Returns a JSON obeject describing which schools and chapters the user has access to
 ***/
export const getPermissions = request => {

	if (process.env.NO_AUTH) {
		return {
			canRead: true,
			canWrite: true,
			canComment: true,
			canLike: true,
			isAdmin: true,
		}
	}

	const {
		canRead,
		canWrite,
		canComment,
		canLike,
		isAdmin
	} = request.user;

	return {
		canRead,
		canWrite,
		canComment,
		canLike,
		isAdmin,
	};
}

/***
 * @description Handles return a callback function to be used by a SQL query
 ***/
export const returnSQLResponse = (connection, response, callback) => {
	return (error, results) => {
		if (response)
			connection.release();
		if (error) {
			console.log(error);
			if (!response && callback) {
				return callback(error, results);
			}
			response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(JSON.stringify(error));
		} else {
			if (!response && callback) {
				return callback(error, results);
			}
			response.json(callback ? callback(results) : results);
		}
	}
}

/***
 * @description Transforms a change set (i.e. {col_to_change: new_value}) into an SQL update string
 * @deprecated Vulnerable to SQL injection
 ***/
export const getUpdateStatementFromChangeSet = changes => {
	let rval = "";
	let SQLChanges = [];

	for (let key in changes) {
		const obj = {};
		obj.key = key;
		obj.val = changes[key];
		SQLChanges.push(obj);
	}

	for (let i = 0; i < SQLChanges.length; i++) {
		rval += SQLChanges[i].key.replace(/'/g, "\\'") + " = '" + (typeof SQLChanges[i].val == "string" ? SQLChanges[i].val.replace(/'/g, "\\'") : SQLChanges[i].val) + "'"
		if (i != SQLChanges.length - 1) {
			rval += ", "
		}
	}

	return rval;
}

/***
 * @description Gets the min date and max date for the current year of ministry as moment objects
 */
export const getCurrentYearOfMinistry = () => {
	const current_date = new Date();

	let current_month = current_date.getMonth();

	let max_date;
	let min_date;

	if (current_month < 6) {
		min_date = `${current_date.getFullYear() - 1}-07-01`;
		max_date = `${current_date.getFullYear()}-06-30`;
	}
	else {
		min_date = `${current_date.getFullYear()}-07-01`;
		max_date = `${current_date.getFullYear() + 1}-06-30`;
	}

	return {
		max_date: moment(max_date),
		min_date: moment(min_date)
	};
}

export const unauthorized = response => {
	response.status(STATUS_CODES.UNAUTHORIZED).json({ reason: 'You are not authorized to do this' });
}

export const serverError = (response, err) => {
	response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({ err });
}

/**
 * 
 * @param {object} connection A connection to the database
 * @param  {...any} params Params to be passed to the connection 
 */
export const promiseQuery = (connection, ...params) => {
	return new Promise((resolve, reject) => {
		connection.query(...params, (err, results) => {
			if (err) {
				return reject(err);
			}
			resolve(results);
		})
	})
}

/**
 * @description Makes a get request to the story capture api
 * @param {string} relativeUrl The relative api path to make the request to
 * @returns {Promise} If resolved: The JSON results of the get request
 */
export const getLocal = relativeUrl => {
	return new Promise((resolve, reject) => {
		http.get(`http://localhost:${process.env.PORT || 8083}${relativeUrl}`, (resp) => {
			let data = '';
			resp.on('data', (chunk) => {
				data += chunk;
			});
			resp.on('end', () => {
				resolve(JSON.parse(data));
			});

		}).on("error", (err) => {
			reject(err);
		});
	})
}