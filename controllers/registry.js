import registerUserRoutes from './user';
import registerStoryRoutes from './story';
import registerAuthenticationRoutes from './authentication';

export default app => {
	registerAuthenticationRoutes(app);
	registerUserRoutes(app);
	registerStoryRoutes(app);
}