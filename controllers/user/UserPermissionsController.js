import STATUS_CODES from 'http-status-codes';
import {
	getPermissions,
	returnSQLResponse
} from '../util';

const deletePermissionsForUser = (request, response, connection, callback) => {
	const {
		permissionIds
	} = request.body;
	const { id } = request.params;

	connection.query(`
		DELETE FROM UserHasPermissions
		WHERE user_id = :id AND permission_id IN (:permissionIds)
	`,
	{id, permissionIds},
	returnSQLResponse(connection, response, callback));
}

const addPermissionsForUser = (request, response, connection, callback) => {
	const {
		permissionIds
	} = request.body;

	const {
		id
	} = request.params

	const permissionSQLObject = {};
	const permissionIdsInsertStatement = permissionIds.reduce ?
		permissionIds.reduce((accumulator, value, index, src) => {
			let rVal = accumulator + `(:id, :permission${value})`;
			if (index < src.length - 1) {
				rVal += ','
			}
			permissionSQLObject[`permission${value}`] = value;
			return rVal;
		}, '')
	:
		'()'

	connection.query(`
		INSERT IGNORE INTO UserHasPermissions
			(user_id, permission_id)
		VALUES
			${permissionIdsInsertStatement}
	`,
	{id, ...permissionSQLObject},
	returnSQLResponse(connection, response, callback));
}

/***
 * @description Utility method that wraps the other two utility methods in a promise so it can be executed async
 */
const wrapInPromise = (method, connection, permissionIds, id) => {
	return new Promise((resolve, reject) => {
		method({body: {permissionIds}, params: {id}}, undefined, connection, (err, results) => {
			if (err) {
				reject(err);
			}
			resolve(results);
		});
	});
}

/***
 * @description Utility method for alterPermissionsForUser route
 */
const updatePermissionsForUser = (request, response, connection) => {
	const {
		permissionIds
	} = request.body;
	const {id} = request.params;

	connection.query(`
		SELECT * FROM UserHasPermissions WHERE user_id = :id
	`,
	{id},
	(err, results) => {
		if (err) {
			return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({err});
		}

		const permissionsToDelete = [];

		for (let i = 0; i < results.length; i++) {
			const pid = results[i].permission_id;
			if (!permissionIds.includes(pid)) {
				permissionsToDelete.push(pid);
			}
		}

		const promises = [
			wrapInPromise(deletePermissionsForUser, connection, permissionsToDelete, id),
			wrapInPromise(addPermissionsForUser, connection, permissionIds, id),
		];

		Promise.all(promises)
		.then(results => {
			connection.release();
			return response.status(STATUS_CODES.OK).json({
				deleted: results[0].affectedRows,
				added: results[1].affectedRows,
			});
		})
		.catch(err => {
			connection.release();
			return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json({
				err
			})
		});
	})
}

/***
 * @description Post method that allows admin users to set permissions for other users
 * @method delete Deletes all permissions in permissionIds
 * @method add Adds all permissions in permissionIds
 * @method update Adds all permissions in permissionIds and deletes all others
 */
export const alterPermissionsForUser = (request, response, connection) => {
	const {
		method,
	} = request.body;

	const { isAdmin } = getPermissions(request);
	if (!isAdmin) {
		return response.status(STATUS_CODES.UNAUTHORIZED).json({reason: 'You are not an admin user'});
	}

	if (method === 'delete') {
		return deletePermissionsForUser(request, response, connection);
	}
	else if (method === 'add') {
		return addPermissionsForUser(request, response, connection);
	}
	else if (method === 'update') {
		updatePermissionsForUser(request, response, connection);
	}
	else {
		return response.status(STATUS_CODES.BAD_REQUEST).json({reason: 'Invalid value for method: must be "add" or "delete"'});
	}
}

/***
 * @description Get method that returns the permissions for a given user
 */
export const viewPermissionsForUser = (request, response, connection) => {
	const {
		id
	} = request.params

	const { isAdmin } = getPermissions(request);
	const loggedInUser = request.user.userId;

	if (!isAdmin && id !== loggedInUser) {
		return response.status(STATUS_CODES.FORBIDDEN).json({reason: 'You are not an admin user'});
	}

	connection.query(
		`
			SELECT Permission.name, Permission.description, Permission.permission_id
			FROM UserHasPermissions
			JOIN
				Permission ON Permission.permission_id = UserHasPermissions.permission_id
			WHERE
				user_id = :id
		`,
		{id},
		returnSQLResponse(connection, response)
	);
}

/***
 * @description Get method to view all permissions
 */
export const viewAllPermissions = (request, response, connection) => {
	const { isAdmin } = getPermissions(request);
	if (!isAdmin) {
		return response.status(STATUS_CODES).json({reason: 'You are not an admin user'});
	}
	connection.query(`
		SELECT * FROM Permission
	`,
	returnSQLResponse(connection, response))
}