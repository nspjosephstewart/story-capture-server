import multer from 'multer';

import {
	withDBConnection,
	isLoggedIn,
	withAdminDBConnection,
} from '../util';
import * as userController from './UserController';
import * as permissionController from './UserPermissionsController';

const upload = multer({ storage: multer.memoryStorage() })

export default app => {
	app.route('/api/user/:id')
		.get(isLoggedIn, withDBConnection(userController.getUser));
	
	app.route('/api/user')
		.post(isLoggedIn, withAdminDBConnection(userController.createUser));

	app.route('/api/user')
		.get(isLoggedIn, withDBConnection(userController.getMe));
	
	app.route('/api/user/update-profile-icon')
		.post(isLoggedIn, upload.single('image'), withAdminDBConnection(userController.updateProfileIcon));

	app.route('/api/users')
		.get(isLoggedIn, withDBConnection(userController.getAllUsers));


	app.route('/api/user/:id/permissions/alter')
		.post(isLoggedIn, withAdminDBConnection(permissionController.alterPermissionsForUser));

	app.route('/api/user/:id/permissions')
		.get(isLoggedIn, withDBConnection(permissionController.viewPermissionsForUser));

	app.route('/api/permissions')
		.get(isLoggedIn, withDBConnection(permissionController.viewAllPermissions));
}