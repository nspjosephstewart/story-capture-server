import * as STATUS_CODES from 'http-status-codes';
import {
	returnSQLResponse,
	getCloudinary,
	serverError,
} from '../util';

/***
 * @description A post method that creates a user in the database
 */
export const createUser = (request, response, connection) => {
	const {
		firstName,
		lastName,
		email,
		isHighSchoolStudent,
		schoolId,
		isCampusMentor,
		chapterId
	} = request.body;

	const hsStudentBit = isHighSchoolStudent ? '1' : '0';
	const cmBit = isCampusMentor ? '1' : '0';
	const schoolIdSQL = isHighSchoolStudent ? ':schoolId' : 'NULL';

	connection.query(
		`INSERT INTO User
			(first_name, last_name, email, is_high_school_student, school_id, is_campus_mentor, chapter_id)
		VALUES
			(:firstName, :lastName, :email, ${hsStudentBit}, ${schoolIdSQL}, ${cmBit}, :chapterId)
		`,
		{firstName, lastName, email, schoolId, chapterId},
		(err, results) => {
			if (err) {
				return response.status(STATUS_CODES.INTERNAL_SERVER_ERROR).json(err);
			}

			return response.status(STATUS_CODES.OK).json({id: results.insertId});
		}
	)
}

/***
 * @description A get method that returns profile information about the user
 */
export const getUser = (request, response, connection) => {
	const { id } = request.params;
	
	connection.query(
		`SELECT first_name, last_name, avatar_url FROM User WHERE user_id = :id`,
		{id},
		returnSQLResponse(connection, response)
	);
}

export const getMe = (request, response, connection) => {
	const { userId } = request.user;
	connection.query(
		`SELECT
			first_name,
			last_name,
			avatar_url,
			(
				SELECT COUNT(*) FROM Story WHERE Story.posted_by_user_id = User.user_id
			) AS posted_stories,
			(
				SELECT COUNT(*) FROM Story 
				WHERE Story.posted_by_user_id = User.user_id AND picture_url IS NOT NULL
			) AS posted_pictures
		FROM
			User
		WHERE user_id=:userId`,
		{userId},
		returnSQLResponse(connection, response)
	)
}

export const updateProfileIcon = async (request, response, connection) => {
	const { userId } = request.user;
	const image = request.file;
	try {
		const url = await uploadImage(image);
		connection.query(
			`UPDATE User SET avatar_url=:url WHERE User.user_id=:userId`,
			{url, userId},
			returnSQLResponse(connection, response, results => ({ success: results.affectedRows > 0 }))
		)
	} catch (err) {
		return serverError(response, err);
	}
}

export const getAllUsers = async (request, response, connection) => {
	let { query, count } = request.query;

	if (query)
		query = `%${query}%`
	const limit = isNaN(count) ? 5 : count;
	const queryCondition = query ? 'WHERE CONCAT(User.first_name, " ", User.last_name) LIKE :query OR User.email LIKE :query' : '';

	connection.query(`
		SELECT
			User.first_name, User.last_name, User.email
		FROM
			User
		${queryCondition}
		LIMIT :limit`,
		{query, limit},
		returnSQLResponse(connection, response)
	);
}

const uploadImage = async image => {
	return new Promise((resolve, reject) => {
		const cloudinary = getCloudinary();
		cloudinary.v2.uploader.upload_stream(
			{resource_type: 'raw', folder: 'story-capture/user-images'}, 
			(error, result) => {
				if (error) {
					return reject(error);
				}
				return resolve(result.url);
			}
		).end(image.buffer);
	});
}