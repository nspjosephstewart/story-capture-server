import { OAuth2Strategy as GoogleStrategy } from 'passport-google-oauth';
import GoogleTokenStrategy from 'passport-google-id-token';
import { getSiteUrl } from '../util';
import authConfig from './creds.json';
import findOrCreateUser from './FindOrCreateUser';

export default (passport, pool) => {
	passport.serializeUser((user, done) => {
		done(null, user);
	})

	passport.deserializeUser((user, done) => {
		done(null, user);
	});

	passport.use('google-id-token', new GoogleTokenStrategy({
		clientID: '639644780945-90iub7nurah05j8epnj6p2eclfg6hpvv.apps.googleusercontent.com'
	}, async (parsedToken, googleId, done) => {
		console.log('Google token id');
		const { payload } = parsedToken;
		console.log(parsedToken.payload);
		console.log(googleId);
		console.log('Trying to authenticate');
		const user = await findOrCreateUser(pool, payload.email);
		return done(null, {
			...user,
			email: payload.email
		});
	}));

	passport.use('google', new GoogleStrategy({
		clientID: authConfig.client_id,
		clientSecret: authConfig.client_secret,
		callbackURL: `${getSiteUrl()}/googlecallback`,
		passReqToCallback: true
	},
		(req, token, refreshToken, profile, done) => {
			console.log(token);
			if (req.user) {
				return done(null, req.user);
			}
			process.nextTick(async () => {
				console.log('looking for users');
				const user = await findOrCreateUser(pool, profile.emails[0].value);
				return done(null, {
					...user,
					email: profile.emails[0],
					token,
					refreshToken,
				});
			})
		}
	))
}