import fs from 'fs';
import format from 'string-template';
import { serverError } from '../util';

export const authenticateWithGoogle = passport =>
	passport.authenticate('google', {
		scope:['profile','email'],
		projection: 'full',
	})

export const googleCallbackHook = passport =>
	passport.authenticate('google', {
		successRedirect : '/',
		failureRedirect : '/login',
	});

export const connectToGoogle = passport => passport.authenticate('google-id-token')

export const logout = (request, response) => {
	request.logout();
	response.end();
}

export const isAuthenticated = (request, response) => {
	return response.json({ authenticated: request.isAuthenticated() });
}

const readFile = file => {
	return new Promise((resolve, reject) => {
		fs.readFile(file, 'utf8', (err, data) => {
			if (err) {
				return reject(err);
			}
			resolve(data);
		})
	});
}