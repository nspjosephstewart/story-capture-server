import {
	promiseQuery
} from '../util';

export default (pool, email) => {
	return new Promise((resolve, reject) => {
		pool.getConnection(
			async (err, connection) => {
				if (err) {
					connection.release();
					reject(err);
				} else {
					try {
						const user = await findUser(connection, email);
						connection.release();
						return resolve(user);
					} catch (err2) {
						connection.release();
						return reject(err2);
					}
				}
			}
		)
	})
}

const permissionIds = {
	canRead: 4,
	canWrite: 1,
	canComment: 2,
	canLike: 3,
	isAdmin: 5,
}

const hasPermission = (name, permissions) => {
	return permissions.includes(permissionIds[name]);
}

const findUser = async (connection, email) => {
	try {
		const userLookupResult = await promiseQuery(
			connection,
			`SELECT user_id FROM User WHERE email=:email`,
			{email}
		)

		let userId;
		let firstTimeLogin = false;
		if (!userLookupResult[0]) {
			userId = createUser(connection, email, firstName, lastName);
		} else {
			userId = userLookupResult[0].user_id;
		}

		const permissions = (await promiseQuery(
			connection,
			`SELECT permission_id FROM UserHasPermissions WHERE user_id=:userId`,
			{userId}
		)).map(obj => obj.permission_id);
		console.log(permissions);

		return {
			userId,
			canRead: hasPermission('canRead', permissions),
			canWrite: hasPermission('canWrite', permissions),
			canComment: hasPermission('canComment', permissions),
			canLike: hasPermission('canLike', permissions),
			isAdmin: hasPermission('isAdmin', permissions),
			firstTimeLogin,
		}
	} catch (err) {
		console.log(err);
		return null;
	}
}

const createUser = async (connection, email, firstName, lastName) => {
	
	const hashedPassword = await bcrypt.hash(password, 8);
	
	const createResult = await promiseQuery(
		connection,
		`INSERT INTO User
			(email, first_name, last_name)
		VALUES
			(:email, :firstName, :lastName)`,
		{email, firstName, lastName, hashedPassword}
	);
	return createResult.insertId;
}