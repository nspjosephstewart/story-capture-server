import passport from 'passport';
import { getConnectionPool } from '../util';
import initPassport from './PassportIntializer';
import {
	authenticateWithGoogle,
	googleCallbackHook,
	logout,
	connectToGoogle,
	isAuthenticated
} from './AuthenticationController';
import session from 'express-session';

export default app => {
	initPassport(passport, getConnectionPool());
	app.use(session({secret:"everystudent2017"}));
	app.use(passport.initialize());
	app.use(passport.session());

	app.route("/logingoogle")
		.get(authenticateWithGoogle(passport))

	app.route('/googlecallback')
		.get(googleCallbackHook(passport));

	app.route("/logout")
		.get(logout);

	app.route('/connect')
		.post(connectToGoogle(passport), (request, response) => {
			console.log(request.user);
			return response.send(request.user ? 201 : 204)
		});

	app.route('/isauthenticated')
		.get(isAuthenticated);
}