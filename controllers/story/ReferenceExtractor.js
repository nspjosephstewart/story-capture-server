// Matches the pattern @{type:id:text}
const regex = /@{(user|school|chapter):([0-9]+):([ a-zA-Z0-9!@#$%^&*()\-=]+)}/g

export default text => {
	const rVal = {
		chapters: [],
		users: [],
		schools: [],
	}
	let match;
	while (match = regex.exec(text)) {
		rVal[`${match[1]}s`].push(match[2]);
	}
	return rVal;
}