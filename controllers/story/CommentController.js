import {
	getPermissions,
	unauthorized,
	returnSQLResponse
} from '../util';

/**
 * @description Gets comments on a particular story
 */
export const getCommentsForStory = (request, response, connection) => {
	const {
		storyId
	} = request.params;
	const {
		count,
		page,
	} = request.query;

	const {
		canRead
	} = getPermissions(request);

	if (!canRead) {
		return unauthorized(response);
	}

	let limit = isNaN(count) ? 10 : parseInt(count);
	if (!isNaN(page)) {
		limit *= parseInt(page);
	}

	connection.query(
		`SELECT comment_id, text, timestamp, first_name, last_name, avatar_url 
		FROM
			UserCommentsOnStory
			JOIN
				User ON UserCommentsOnStory.user_id = User.user_id
		WHERE story_id=:storyId
		ORDER BY timestamp DESC
		LIMIT :limit`,
		{limit, storyId},
		returnSQLResponse(connection, response)
	);
}

/**
 * @description Adds a comment for a particular story
 */
export const commentOnStory = (request, response, connection) => {
	const { storyId } = request.params;
	const { text } = request.body;
	const { userId } = request.user;

	const {
		canComment
	} = getPermissions(request);

	if (!canComment) {
		return unauthorized(response);
	}

	connection.query(
		`INSERT INTO UserCommentsOnStory
		(story_id, user_id, text)
		VALUES
		(:storyId, :userId, :text)`,
		{storyId, userId, text},
		returnSQLResponse(connection, response, results => ({commentId: results.insertId}))
	);
}

/**
 * @description Delete a comment for a story
 */
export const deleteCommentForStory = (request, response, connection) => {
	const { commentId, storyId } = request.params;
	const { userId } = request.user;
	
	let {
		isAdmin
	} = getPermissions(request);
	isAdmin = isAdmin || false;
	
	connection.query(
		`DELETE FROM UserCommentsOnStory
		WHERE comment_id = :commentId AND story_id=:storyId AND (user_id=:userId OR ${isAdmin})`,
		{commentId, storyId, userId},
		returnSQLResponse(connection, response, results => ({ deletedComments: results.affectedRows }))
	);
}

/**
 * @description View a specific comment
 */
export const getCommentForStory = (request, response, connection) => {
	const { commentId, storyId } = request.params;
	let { canRead } = getPermissions(request);

	if (!canRead) {
		return unauthorized(response);
	}

	connection.query(
		`SELECT comment_id, text, timestamp, first_name, last_name, avatar_url 
		FROM
			UserCommentsOnStory
			JOIN
				User ON UserCommentsOnStory.user_id = User.user_id
		WHERE story_id=:storyId AND comment_id=:commentId`,
		{storyId, commentId},
		returnSQLResponse(connection, response),
	)
}