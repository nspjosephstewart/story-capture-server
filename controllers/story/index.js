import multer from 'multer';

import {
	withDBConnection,
	isLoggedIn,
	withAdminDBConnection,
} from '../util';
import * as storyRoutes from './StoryController';
import * as commentRoutes from './CommentController';
import * as likeRoutes from './LikeController';

const upload = multer({ storage: multer.memoryStorage() });

export default app => {
	// Story routes
	app.route('/api/stories')
		.get(isLoggedIn, withDBConnection(storyRoutes.getStories));

	app.route('/api/stories')
		.post(isLoggedIn, upload.single('image'), withAdminDBConnection(storyRoutes.postStory));

	app.route('/api/stories/:storyId')
		.delete(isLoggedIn, withAdminDBConnection(storyRoutes.deleteStory));

	app.route('/api/stories/:storyId')
		.get(isLoggedIn, withDBConnection(storyRoutes.getStory));


	// Comment routes
	app.route('/api/stories/:storyId/comments')
		.get(isLoggedIn, withDBConnection(commentRoutes.getCommentsForStory));

	app.route('/api/stories/:storyId/comments')
		.post(isLoggedIn, withAdminDBConnection(commentRoutes.commentOnStory));
	
	app.route('/api/stories/:storyId/comments/:commentId')
		.delete(isLoggedIn, withAdminDBConnection(commentRoutes.deleteCommentForStory));

	app.route('/api/stories/:storyId/comments/:commentId')
		.get(isLoggedIn, withDBConnection(commentRoutes.getCommentForStory));


	// Like routes
	app.route('/api/stories/:storyId/like')
		.get(isLoggedIn, withAdminDBConnection(likeRoutes.likeStory));

	app.route('/api/stories/:storyId/unlike')
		.get(isLoggedIn, withAdminDBConnection(likeRoutes.unlikeStory));
}