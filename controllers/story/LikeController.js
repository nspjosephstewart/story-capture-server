import {
	getPermissions,
	unauthorized,
	returnSQLResponse
} from '../util';

/**
 * @description Current user likes a story
 */
export const likeStory = (request, response, connection) => {
	const { storyId } = request.params;
	const { userId } = request.user;
	const { canLike } = getPermissions(request);

	if (!canLike) {
		return unauthorized(response);
	}

	console.log('Attempting to like story');
	console.log(`User is logged in as user ${userId}`);

	connection.query(
		`INSERT IGNORE INTO UserLikesStory
			(story_id, user_id)
		VALUES
			(:storyId, :userId)`,
		{storyId, userId},
		returnSQLResponse(connection, response, results => {
			console.log(results);
			return ({ liked: (results.affectedRows > 0) })
		})
	);
}

/**
 * @description Current user unlikes a story
 */
export const unlikeStory = (request, response, connection) => {
	const { storyId } = request.params;
	const { userId } = request.user;

	console.log('Attempting to unlike story');
	console.log(`User is logged in as user ${userId}`);

	connection.query(
		`DELETE FROM UserLikesStory WHERE story_id=:storyId AND user_id=:userId`,
		{storyId, userId},
		returnSQLResponse(connection, response, results => ({ unliked: (results.affectedRows > 0) }))
	);
}