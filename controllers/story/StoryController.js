import * as STATUS_CODES from 'http-status-codes';
import {
	getPermissions,
	returnSQLResponse,
	unauthorized,
	serverError,
	promiseQuery,
	getLocal,
	getCloudinary,
} from '../util';
import extractReferences from './ReferenceExtractor';

/***
 * @description Retrieves the story feed for the app
 */
export const getStories = (request, response, connection) => {
	const {
		page,
		count
	} = request.query;

	const {
		canRead
	} = getPermissions(request);

	if (!canRead) {
		return unauthorized(response);
	}

	let limit = isNaN(count) ? 10 : parseInt(count);
	if (!isNaN(page)) {
		limit *= parseInt(page);
	}

	connection.query(
		`
		SELECT 
			story_id,
			text,
			picture_url,
			user_id,
			first_name,
			last_name,
			avatar_url,
			timestamp,
			(
				SELECT COUNT(*) 
				FROM UserLikesStory 
				WHERE UserLikesStory.story_id=Story.story_id
			) AS likes,
			(
				SELECT COUNT(comment_id)
				FROM UserCommentsOnStory
				WHERE UserCommentsOnStory.story_id=Story.story_id
			) AS comments,
			(
				SELECT user_id IN (
					SELECT UserLikesStory.user_id
					FROM UserLikesStory
					WHERE UserLikesStory.story_id=Story.story_id
				)
			) AS user_likes_story
		FROM
			Story
				JOIN
			User ON posted_by_user_id = user_id
		ORDER BY Story.timestamp DESC
		LIMIT :limit
		`,
		{limit},
		returnSQLResponse(connection, response)
	)
}

const postUserReferences = async (connection, users, storyId) => {
	const promises = [];
	for (let i = 0; i < users.length; i++) {
		promises.push(
			promiseQuery(
				connection,
				`INSERT INTO StoryReferencesUser
				(story_id, user_id)
				VALUES
				(:storyId, :userId)`,
				{storyId, userId: users[i]}
			)
		);
	}
	return await Promise.all(promises);
}

const postChapterReferences = async (connection, chapters, storyId) => {
	const promises = [];
	for (let i = 0; i < chapters.length; i++) {
		promises.push(
			promiseQuery(
				connection,
				`INSERT INTO StoryReferencesChapter
				(story_id, chapter_id)
				VALUES
				(:storyId, :chapterId)`,
				{storyId, chapterId: chapters[i]}
			)
		);
	}
	return Promise.all(promises);
}

const postSchoolReferences = async (connection, schools, storyId) => {
	const promises = [];
	for (let i = 0; i < schools.length; i++) {
		promises.push(
			promiseQuery(
				connection,
				`INSERT INTO StoryReferencesSchool
				(story_id, school_id)
				VALUES
				(:storyId, :schoolId)`,
				{storyId, schoolId: schools[i]}
			)
		);
	}
	return Promise.all(promises);
}

/**
 * @description Posts a new story and extraxts user, school, and chapter references using @-tagging syntax
 */
export const postStory = async (request, response, connection) => {
	const {
		text
	} = request.body;
	const { userId } = request.user;
	const image = request.file;

	const { canWrite } = getPermissions(request);
	if (!canWrite) {
		return unauthorized(response);
	}

	console.log(image);

	let imageUrl;
	if (image) {
		try {
			imageUrl = await uploadImage(image);
		} catch {
			imageUrl = undefined;
		}
	}

	try {
		const postedStory = await promiseQuery(connection,
			`INSERT INTO Story
			(text, picture_url, posted_by_user_id)
			VALUES
			(:text, :image, :userId)`,
			{text, image: imageUrl, userId}
		);

		const storyId = postedStory.insertId;

		const {
			chapters,
			users,
			schools
		} = extractReferences(text);

		await postUserReferences(connection, users, storyId);
		await postSchoolReferences(connection, schools, storyId);
		await postChapterReferences(connection, chapters, storyId);

		return response.status(STATUS_CODES.OK).json({storyId, usersReferenced: users ? users.length : 0});
	} catch (err) {
		console.log(err);
		return serverError(response, err);
	}
}

/***
 * @description Deletes a story if the current user posted the story in question or if the user is an admin
 */
export const deleteStory = async (request, response, connection) => {
	const { storyId } = request.params;
	const { userId } = request.user;

	if (!storyId) {
		return response.status(STATUS_CODES.BAD_REQUEST).json({err: 'Must provide story id'});
	}

	let {
		isAdmin
	} = getPermissions(request);
	isAdmin = isAdmin || false;

	try {
		const queryResponse = await promiseQuery(
			connection,
			`DELETE FROM Story WHERE story_id = :storyId AND (posted_by_user_id = :userId OR ${isAdmin})`,
			{storyId, userId}
		);
		return response.status(STATUS_CODES.OK).json({deletedStories: queryResponse.affectedRows});
	} catch (err) {
		return serverError(response, err);
	}
}

/***
 * @description Views a specific story given by the url parameter storyId
 */
export const getStory = async (request, response, connection) => {
	const { storyId } = request.params;
	const {
		canRead
	} = getPermissions(request);

	if (!canRead) {
		return unauthorized(response);
	}

	try {
		const story = await promiseQuery(connection,
			`SELECT 
				story_id,
				text,
				picture_url,
				user_id,
				first_name,
				last_name,
				avatar_url,
				(
					SELECT COUNT(*) 
					FROM UserLikesStory 
					WHERE UserLikesStory.story_id=Story.story_id
				) AS likes,
				(
					SELECT user_id IN (
						SELECT UserLikesStory.user_id
						FROM UserLikesStory
						WHERE UserLikesStory.story_id=Story.story_id
					)
				) AS user_likes_story
			FROM
				Story
					JOIN
				User ON posted_by_user_id = user_id
			WHERE story_id = :storyId`,
			{storyId}
		);

		const commentsOnStory = await getLocal(`/api/stories/${storyId}/comments`);
		if (story[0]) {
			story[0].comments = commentsOnStory;
		}
		
		return response.status(STATUS_CODES.OK).json(story);
	} catch (err) {
		return serverError(response, err);
	}
}

const uploadImage = async image => {
	return new Promise((resolve, reject) => {
		const cloudinary = getCloudinary();
		cloudinary.v2.uploader.upload_stream(
			{resource_type: 'raw', folder: 'story-capture'}, 
			(error, result) => {
				if (error) {
					return reject(error);
				}
				return resolve(result.url);
			}
		).end(image.buffer);
	});
}