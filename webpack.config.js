var fs = require('fs')
var path = require('path')

module.exports = {
  entry: ['babel-polyfill', path.resolve(__dirname, 'server.js')],

  output: {
	path: __dirname,
	filename: './server.bundle.js'
  },

  target: 'node',

  // keep node_module paths out of the bundle
  externals: fs.readdirSync(path.resolve(__dirname, 'node_modules')).concat([
	'react-dom/server'
  ]).reduce(function (ext, mod) {
	ext[mod] = 'commonjs ' + mod
	return ext
  }, {}),

  node: {
	__filename: false,
	__dirname: false
  },

  module: {
	rules: [
		{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader',
			query: {
				presets:['@babel/preset-env']
			}
		}
	]
  }

}
