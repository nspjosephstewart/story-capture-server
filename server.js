import express from 'express';
import bodyParser from 'body-parser';
import registerRoutes from './controllers/registry';

const app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

if (process.env.NO_AUTH) {
	app.use((request, response, next) => {
		request.user = { userId: 1 };
		return next();
	})
}

registerRoutes(app);

app.listen(process.env.PORT || 8083);
console.log(`LISTENING ON PORT ${process.env.PORT || 8083}`);
